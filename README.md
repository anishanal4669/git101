# git101

# git101

What is the single most important tool that Developers and Operations teams must know alike today? Git! Git is a distributed version control system that allows developers and operations teams to collaborate and keep track of the changes made on a project. GIT, as a DevOps tool, empowers collaboration and faster release cycles. Anyone willing to start their DevOps career or level up should start from the basics, and GIT is the most fundamental requirement of all.


### What is GIT ?
GIT is a open source distributed version control system (VCS) used for tracking changes in source code and coordinating work among multiple developers. So in short it is used by content tracking It was developed by Linus Torvalds in 2005 and has become one of the most widely used version control systems in the software development industry.

The main purpose of GIT is to enable collaboration and efficient management of software development projects. It allows developers to work simultaneously on the same codebase, track changes, and merge their work seamlessly.

### How can I use GIT ?

There are multiple ways to use GIT --> You can work with GIT using Git Bash, Git Desktop, GIT GUI and many more tools. Today we will work with GIT using GIT bash

For that You need to install GIT Bash first. and once you done with you need to setup your identity.
 This is important because every Git commit uses this information, and it’s immutably baked into the commits you start creating:

$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com

Again, you need to do this only once if you pass the --global option, because then Git will always use that information for anything you do on that system. If you want to override this with a different name or email address for specific projects, you can run the command without the --global option when you’re in that project.

### Is this enough now to work with Gitlab ?
No, You need to setup your identity over Gitlab. How to do that ?
YOu would need to setup ssh keys.

SSH keys, short for Secure Shell keys, are a pair of cryptographic keys used for secure authentication and communication between a client and a server over a network. The Secure Shell (SSH) protocol is widely used for secure remote access to servers and for secure file transfers.

SSH keys are an alternative authentication method to traditional username and password combinations. They provide a more secure way of logging into a remote server because they are based on public-key cryptography.

Here's how SSH keys work:

Key Generation: When a user wants to use SSH keys for authentication, they first generate a pair of keys on their local machine – a private key and a public key. The private key should be kept secure and never shared, while the public key can be freely distributed.

Command to generate SSH keys

`ssh-keygen -t rsa`


Public Key Placement: The user then copies their public key to the remote server they want to access. This is usually done by adding the public key to the ~/.ssh/authorized_keys file on the Gitlab. Each user's public key is stored in this file, and the server uses it to verify the user's identity during the authentication process.


### What is GIT local & remote repository ?

In Git, a local repository and a remote repository are two essential components of the version control system that work together to manage the source code of a project.

1. Local Repository:
A local repository is a copy of the entire project's history and files stored on a developer's local machine. When you initialize a local repository, Git sets up a hidden directory called ".git" in the root of the project directory. This directory contains all the necessary information to track changes, manage branches, and perform version control operations.

When you make changes to files in the local repository, you go through a series of steps:

- **Working Directory**: This is the directory on your local machine where you make changes to your files.
- **Staging Area (Index)**: After making changes, you add the modified files to the staging area to prepare them for the next commit.
- **Commit**: When you commit the changes, Git takes a snapshot of the files in the staging area and permanently stores them in the local repository with a unique identifier (commit hash). Each commit in the history records the changes made to the files.

The local repository allows you to work on your code, review the history of changes, and revert to previous versions if needed, all without the need for an internet connection.

2. Remote Repository:
A remote repository is a version of the project's repository hosted on a remote server, often on a code hosting platform like gitlab, GitLab, or Bitbucket. The remote repository serves as a central location where multiple developers can collaborate, share code, and contribute to the project.

When you want to share your code changes with others or backup your work, you push your local commits to the remote repository. Similarly, when other developers make changes in the remote repository, you can pull those changes into your local repository to stay up-to-date with the latest codebase.

Collaboration with remote repositories typically follows these steps:

- **Push**: To share your local commits with the remote repository, you push them from your local repository to the corresponding branch in the remote repository.
- **Pull**: To update your local repository with the latest changes from the remote repository, you pull the changes from the remote repository to your local branch.

Remote repositories allow developers to work together, manage merge conflicts, review each other's code, and maintain a centralized version of the project.

In summary, the local repository is your personal workspace where you make changes and commit them, while the remote repository is the central shared repository where you collaborate with other developers and synchronize your work.

### How can I initialize a local GIT repository ?

To initialize a local Git repository, follow these steps:

1. Open your terminal or command prompt on your local machine.

2. Navigate to the directory where you want to create the Git repository. You can use the `cd` (change directory) command to move to the desired location. For example:
   ```
   cd /path/to/your/project
   ```

3. Once you are in the desired directory, use the following command to initialize the Git repository:
   ```
   git init
   ```

   This command creates a new `.git` directory in your project folder. The `.git` directory is the core of the Git repository and contains all the necessary information for version control.

4. Your local Git repository is now initialized. You can start adding files, making changes, and creating commits to track your project's history.

To add files to the repository, use the following command:
```
git add <file1> <file2> ...
```

To commit the changes and save them to the repository, use the following command:
```
git commit -m "Your commit message here"
```

Replace `<file1>`, `<file2>`, etc., with the names of the files you want to add, and replace `"Your commit message here"` with a meaningful description of the changes you made in the commit.

After the initial commit, you can continue to add, modify, and commit files as needed to keep track of your project's progress.

Keep in mind that this is only a local Git repository on your machine. If you want to collaborate with others or back up your code to a remote repository (e.g., on Gitlab), you'll need to create a remote repository and push your local repository to it.

### How to initialize a remote repository in GIT ?

To initialize a remote repository in Git, you typically use a code hosting platform like gitlab, GitLab, or Bitbucket. These platforms provide web-based interfaces that allow you to create remote repositories and manage your code in a collaborative environment. Below are the general steps to initialize a remote repository using gitlab as an example:

1. **Create an Account**: If you don't have an account on gitlab, go to https://gitlab.com/ and sign up for a new account.

2. **Create a New Repository**:
   - Once you are logged in, click on the "+" sign in the top right corner of the page, and then select "New repository".
   - Give your repository a name and optional description.
   - Choose whether the repository should be public (visible to everyone) or private (visible only to you or collaborators with access).
   - Optionally, you can initialize the repository with a README file, which is helpful to provide initial information about your project.

3. **Create the Repository**:
   - Click the "Create repository" button, and your remote repository will be created.

4. **Get the Remote Repository URL**:
   - After creating the repository, you'll be redirected to the repository's page on gitlab.
   - Find the "HTTPS" or "SSH" URL of your repository. You'll use this URL to connect your local repository to the remote one.

5. **Link the Local Repository to the Remote Repository**:
   - On your local machine, navigate to the root directory of the project (the directory containing your code).
   - If you've already initialized a local Git repository (using `git init`), skip this step. Otherwise, if you haven't initialized a local repository yet, you can do so using `git init` as explained in the previous response.

6. **Set Up the Remote Origin**:
   - Use the following command to link your local repository to the remote repository on gitlab:
     ```
     git remote add origin <remote_repository_url>
     ```
   - Replace `<remote_repository_url>` with the HTTPS or SSH URL you obtained from gitlab.

7. **Push Your Local Repository to the Remote Repository**:
   - Now that your local repository is connected to the remote repository, you can push your local code to the remote repository using the following command:
     ```
     git push --set-upstream origin master
     git push -u origin master
     ```
   - This command pushes your current branch (usually the "master" branch) to the "origin" remote repository. The `-u` flag is used to set the upstream branch, which means that in the future, you can simply use `git push` without specifying the branch name.

Your local repository is now linked to the remote repository on gitlab, and you can collaborate with others, share your code, and keep your codebase backed up on the remote server.

### What is GIT branch ? How can I create a new branch and work with it ?

In Git, a branch is a separate line of development that allows you to work on different features or changes in isolation. Branches are lightweight and enable you to experiment with new ideas without affecting the main codebase. Each branch has its own commits, meaning you can make changes, create commits, and switch between branches seamlessly.

The main branch in a Git repository is typically called "master" or "main." When you create a new repository, you automatically start with the main branch.

To create a new branch and work with it, follow these steps:

1. **Check Current Branch**:
   Before creating a new branch, it's a good practice to check which branch you are currently on. You can use the following command to see the current branch:
   ```
   git branch
   ```

2. **Create a New Branch**:
   To create a new branch, use the `git branch` command followed by the branch name you want to create. For example, to create a branch named "feature-branch," use:
   ```
   git branch feature-branch
   ```

3. **Switch to the New Branch**:
   To start working on the new branch, you need to switch to it. Use the `git checkout` command with the name of the branch you want to switch to:
   ```
   git checkout feature-branch
   ```
   Alternatively, you can use the `git switch` command (Git version 2.23 or later) as follows:
   ```
   git switch feature-branch
   ```

   Note: In recent versions of Git, the `git switch` command is preferred over `git checkout` for creating and switching branches to reduce confusion between different use cases of `git checkout`.

4. **Work on the New Branch**:
   Now that you are on the new branch, you can make changes to your code, add new files, and create commits as usual using `git add` and `git commit`.

5. **Push the Branch to Remote (Optional)**:
   If you want to share your new branch with others or back it up on a remote repository, you can push it using the following command:
   ```
   git push -u origin feature-branch
   ```
   This command will push the branch to the remote repository (assuming "origin" is the name of your remote) and set it as the upstream branch. After this step, you can use `git push` without specifying the branch name.

6. **Switching Between Branches**:
   To switch between branches, use the same `git checkout` or `git switch` commands followed by the name of the branch you want to switch to. Any uncommitted changes in your working directory will be carried over to the new branch.

7. **Merge Changes (Optional)**:
   Once you are done working on the new branch and you want to include the changes in the main branch (or any other branch), you can merge the new branch into the target branch using the `git merge` command.

That's it! You have created a new branch, worked on it, and optionally merged it back into the main branch or any other target branch. Branching is a powerful feature in Git that enables collaborative development and effective code management.

### How to compare two branches in GIT ?

To compare two branches in Git and see the differences between them, you can use the `git diff` command. This command shows the changes that exist between two branches or any two Git objects, such as commits, branches, or tags.

Here's how you can compare two branches in Git:

1. **Ensure that you are on the branch you want to compare from**:
   Before comparing branches, ensure that you are on the branch from which you want to compare the changes. You can use the `git checkout` or `git switch` command to switch to the desired branch. For example:
   ```
   git checkout branch1
   ```

2. **Run the `git diff` command**:
   To compare the current branch (branch1) with another branch (branch2), use the following command:
   ```
   git diff branch2
   ```
   This will display the differences between the two branches. The output will show the added lines, removed lines, and modified lines between the two branches.

3. **Compare Specific Commits**:
   If you want to compare specific commits rather than entire branches, you can use their commit hashes. For example, to compare two specific commits, commit1 and commit2, you can use the command:
   ```
   git diff commit1 commit2
   ```
   This will show the differences between the two specified commits.

4. **Compare with Previous Commit**:
   To see the changes between the current branch and the previous commit (HEAD), you can use the following command:
   ```
   git diff HEAD
   ```
   This command will show the differences between the current branch and the last commit.

5. **View Differences in a Specific File**:
   If you only want to compare changes in a specific file between branches, you can specify the file's name after the branch names or commit hashes. For example:
   ```
   git diff branch1 branch2 path/to/file
   ```
   This will display the differences for the specified file between the two branches.

Remember that the `git diff` command only shows the differences in the working directory. If you have uncommitted changes in the working directory, it will compare those changes as well. If you want to compare the differences between two commits without considering any local changes, you can create a temporary branch and use the `git diff` command to compare that specific branch with the target branch or commit.

With the `git diff` command, you can easily compare and understand the changes between branches, commits, or individual files in your Git repository.

### How can I rebase my branch with master branch ?

To rebase your branch with the master branch, you can use the `git rebase` command. Rebasing is a way to integrate the changes from one branch into another by moving or applying the commits of your branch on top of the latest commits from the master branch.

Here's a step-by-step guide to rebase your branch with the master branch:

1. **Ensure you are on your branch**:
   First, make sure you are on the branch you want to rebase. If you are not on the branch, switch to it using the `git checkout` or `git switch` command. For example, if your branch is called "feature-branch," use:
   ```
   git checkout feature-branch
   ```

2. **Fetch the latest changes from the remote** (Optional but recommended):
   Before rebasing, it's a good idea to fetch the latest changes from the remote repository to ensure you have the most up-to-date version of the master branch. Use the following command to fetch the changes:
   ```
   git fetch origin
   ```

3. **Update your master branch (Optional but recommended)**:
   If you haven't updated your master branch lately, you can pull the latest changes from the remote master branch into your local master branch. This step ensures that your local master branch is up-to-date. Use the following command to pull the changes:
   ```
   git pull origin master
   ```

4. **Rebase your branch with the master branch**:
   Now that you have the latest changes from the master branch, you can rebase your branch on top of it. Use the following command to rebase:
   ```
   git rebase master
   ```
   This command takes all the commits from your branch that are not in the master branch and places them on top of the latest commit in the master branch.

5. **Resolve any conflicts (if necessary)**:
   During the rebase, Git might encounter conflicts if there are conflicting changes between your branch and the master branch. If conflicts occur, Git will pause the rebase and ask you to resolve the conflicts manually. Open the conflicting files, resolve the conflicts, save the changes, and then use `git add` to mark the resolved files as resolved.

6. **Continue the rebase**:
   After resolving conflicts, continue the rebase by using the following command:
   ```
   git rebase --continue
   ```

7. **Push the rebased branch**:
   Once the rebase is complete and there are no more conflicts, you can push the rebased branch to the remote repository to update it there. Use the following command:
   ```
   git push -f origin feature-branch
   ```
   Note: The `-f` flag (force) is used to overwrite the remote branch history with the rebased version. Be cautious when using this flag, as it can potentially cause issues for other developers working on the branch.

After completing these steps, your branch should be rebased with the latest changes from the master branch. Rebasing helps keep a linear history and makes the branch's commits more closely aligned with the latest code in the main branch, enhancing code collaboration and making it easier to merge your branch later on.

### How can I resolve merge conflict ?

Resolving a merge conflict in Git involves manually addressing conflicting changes between two branches that are being merged. Git will pause the merge process when it encounters conflicts and mark the conflicted files with special conflict markers. Here's a step-by-step guide to resolving merge conflicts:

1. **Identify the Conflicts**:
   When you attempt to merge two branches, and Git encounters conflicting changes in one or more files, it will display a message indicating that there are conflicts. To identify the conflicted files, you can use the following command:
   ```
   git status
   ```
   Conflicted files will be listed under the "Unmerged paths" section.

2. **Open the Conflicted Files**:
   Open the conflicted files in a text editor or an Integrated Development Environment (IDE). Inside the file, you'll see conflict markers like this:

   ```
   <<<<<<< HEAD
   // Your changes on the current branch
   =======
   // Changes from the branch being merged
   >>>>>>> branch-name
   ```

   The section between `<<<<<<< HEAD` and `=======` represents the changes from your current branch (HEAD), and the section between `=======` and `>>>>>>> branch-name` represents the changes from the branch you are merging (`branch-name`).

3. **Resolve the Conflicts**:
   Manually edit the conflicted file and decide which changes to keep. You can remove the conflict markers and modify the code to make it work as intended. For example, you can choose to keep both sets of changes, combine them, or discard some changes entirely.

4. **Save the Changes**:
   After resolving the conflicts, save the file.

5. **Stage the Resolved Files**:
   Once you have resolved all the conflicts in the file, stage the resolved files to mark them as resolved. Use the following command to stage the changes:
   ```
   git add <resolved-file>
   ```
   Replace `<resolved-file>` with the path to the file you resolved. If you have multiple resolved files, you can use `git add .` to stage all resolved files at once.

6. **Continue the Merge Process**:
   After staging all the resolved files, continue the merge process using the following command:
   ```
   git commit
   ```
   This will complete the merge commit. You can provide a merge commit message or use the default message, which Git will generate for you.

7. **Finish the Merge**:
   If you haven't already done so, complete the merge process by using the following command:
   ```
   git merge --continue
   ```
   Alternatively, you can use `git commit` with the `--no-edit` flag to finalize the merge without editing the merge commit message.

8. **Push the Merged Branch**:
   Finally, push the merged branch to the remote repository using the following command:
   ```
   git push origin <your-branch>
   ```
   Replace `<your-branch>` with the name of your branch.

Congratulations! You've successfully resolved the merge conflict and completed the merge. The conflicting changes have been combined, and your branch is now up-to-date with the latest changes from the merged branch.

### How can I fork a reposirtory ?

Forking a repository in GitLab is a way to create your own copy of another user's or organization's repository. This copy is stored within your own GitLab account, and you can make changes to it without affecting the original repository. Here's how you can fork a repository in GitLab:

1. **Log in to GitLab**: Make sure you are logged in to your GitLab account.

2. **Navigate to the Repository**: Go to the GitLab repository that you want to fork.

3. **Fork the Repository**:
   - On the top right corner of the repository page, you should see a "Fork" button. Click on it.
   - GitLab will prompt you to select where you want to fork the repository. Choose your user account or a specific group you have access to.

4. **Wait for Fork to Complete**: GitLab will now create a copy of the repository under your account or group. Depending on the size of the repository and GitLab's server load, this might take a moment.

5. **Access Your Forked Repository**: Once the fork is complete, you will be redirected to your forked repository's page. The URL will indicate that it's a fork of the original repository.

Now you have your own copy of the repository, and you can make changes, create branches, commit code, and more without affecting the original repository. You can also create merge requests (pull requests) from your fork to the original repository to contribute changes back if desired.

Keep in mind that your fork will not automatically stay in sync with the original repository. If the original repository is updated by its owner, you might want to sync your fork with those changes to keep it up to date. You can do this by adding the original repository as a remote and pulling its changes into your fork.

Additionally, if you're using Git locally on your computer, you can follow these steps to set up your fork as a remote and keep it synchronized with the original repository:

1. **Clone Your Fork Locally**:
   ```
   git clone <your_fork_url>
   cd <repository_name>
   ```

2. **Add the Original Repository as a Remote**:
   ```
   git remote add upstream <original_repository_url>
   ```

3. **Fetch and Merge Changes from the Original Repository**:
   ```
   git fetch upstream
   git merge upstream/main  # or the relevant branch
   ```

These steps will ensure that you can easily pull in changes from the original repository into your fork whenever needed.



### How can i sync my forked repository from target repository in Gitlab ?

To sync your forked repository with changes from the target repository (the original repository you forked from) in GitLab, you need to update your fork with the latest changes made in the target repository. Here's how you can do that:

1. **Add the Original Repository as an Upstream Remote**:
   If you haven't already done so, you need to add the original repository as an "upstream" remote to your local clone of the fork. Open a terminal or command prompt and navigate to your local forked repository directory. Then run the following command, replacing `<original_repository_url>` with the URL of the original repository:
   ```bash
   git remote add upstream <original_repository_url>
   ```

2. **Fetch the Latest Changes from the Upstream Remote**:
   Once you've added the upstream remote, you can fetch the latest changes from the original repository:
   ```bash
   git fetch upstream
   ```

3. **Merge or Rebase the Changes**:
   After fetching the changes, you can choose to merge or rebase the upstream changes into your local branch. Here are two common approaches:

   a. **Merge**:
      ```bash
      git checkout <your_branch_name>
      git merge upstream/main  # or the relevant branch
      ```

   b. **Rebase** (This will rewrite your commit history, so use with caution):
      ```bash
      git checkout <your_branch_name>
      git rebase upstream/main  # or the relevant branch
      ```

   Note: Replace `main` with the name of the branch you want to sync with from the target repository.

4. **Push the Changes to Your Fork on GitLab**:
   After merging or rebasing the changes locally, push the updated branch to your forked repository on GitLab:
   ```bash
   git push origin <your_branch_name>
   ```

5. **Create a Merge Request (Optional)**:
   If you want to contribute the changes you synced with from the original repository back to your fork on GitLab, you can create a merge request. This step might be optional depending on your workflow.

By following these steps, your forked repository should now be synced with the latest changes from the target repository. Remember that if you're using a different branch name, you'll need to adjust the commands accordingly.

Always make sure you understand the consequences of merging or rebasing, especially if you're working in a collaborative environment. Rebasing can rewrite commit history, which might cause conflicts or confusion for others working with your repository.

### What is GIT bisect and when to use it ?
`git bisect` is a powerful Git command that is used for binary search-like debugging. It helps you identify the commit that introduced a bug or a problem in your codebase by systematically narrowing down the range of commits that could potentially be responsible for the issue. This is especially useful when you have a large commit history and need to identify when a bug was introduced.

Here's how `git bisect` works and when to use it:

1. **Starting the Bisecting Process**:
   To start the bisecting process, you need to identify a "good" commit (a commit where the bug was not present) and a "bad" commit (a commit where the bug is present). You use these to establish a range of commits that may contain the problematic commit.

   - Run `git bisect start` to begin the bisecting process.
   - Run `git bisect good <good_commit>` to mark the commit as good.
   - Run `git bisect bad <bad_commit>` to mark the commit as bad.

2. **Testing Commits**:
   Git will automatically check out a commit in the middle of the specified range. You then need to test this commit to determine if the bug is present or not. Based on your testing, you mark the commit as either "good" or "bad".

   - If the commit is good, run `git bisect good`.
   - If the commit is bad, run `git bisect bad`.

3. **Repeat Testing and Marking**:
   Git will continue narrowing down the range by checking out commits in the middle of the remaining range based on your input. You repeat the testing and marking process until Git identifies the specific commit that introduced the bug.

4. **Finishing the Bisect Process**:
   Once `git bisect` has identified the problematic commit, it will print out the commit hash and related information. You can then exit the bisect process using `git bisect reset`, which will return your repository to its original state.

When to use `git bisect`:

Use `git bisect` when you're dealing with a bug or an issue in your codebase and you want to quickly narrow down the commit that introduced the problem. This can be especially useful in situations where you have a complex commit history and it's challenging to manually identify the source of the bug.

Here are some scenarios where `git bisect` can be valuable:

1. **Regression Testing**: When you have a known good state and a known bad state, and you need to find the commit that introduced the regression.

2. **Identifying Bug Introductions**: If you notice a bug in your application and want to pinpoint the commit that introduced it, `git bisect` can help you quickly isolate the problematic commit.

3. **Large Codebases**: In large codebases with extensive commit histories, manually searching for the source of a bug can be time-consuming and error-prone. `git bisect` automates this process.

4. **Collaborative Development**: When multiple developers are working on a project, and a bug is discovered, `git bisect` can help narrow down the commit that needs attention, even if the bug was introduced by someone else.

Keep in mind that using `git bisect` requires some familiarity with the Git command-line interface and your codebase. It's also important to have a clear understanding of what constitutes a "good" and "bad" state, as well as the ability to accurately test commits for the presence of the bug.

### How can I revert my changes in GIT ?

In Git, you can revert changes made in a commit by creating a new commit that undoes the changes introduced in the original commit. This process is different from deleting or removing a commit from history, as it preserves the commit history while effectively canceling out the changes introduced by a specific commit.

To revert changes in Git, follow these steps:

1. **Identify the Commit to Revert**:
   First, use `git log` to view the commit history and find the commit that you want to revert. Each commit will have a unique commit hash, and you can use this hash to specify the commit you want to revert.

2. **Create the Revert Commit**:
   Use the following command to create a new commit that reverts the changes of the specified commit:
   ```
   git revert <commit-hash>
   ```
   Replace `<commit-hash>` with the hash of the commit you want to revert. This will open the default text editor to enter the commit message for the revert. You can modify the message if needed, save, and close the editor.

3. **Resolve Conflicts (if any)**:
   If the revert introduces conflicts (e.g., if the changes you are reverting conflict with other changes in your repository), resolve the conflicts as you would with a regular merge conflict.

4. **Commit the Revert**:
   After resolving any conflicts, stage the changes and commit the revert:
   ```
   git add .         # Stage the changes
   git commit        # Commit the revert
   ```

   The revert commit effectively undoes the changes introduced in the original commit and creates a new commit with the opposite changes.

5. **Push the Revert (if needed)**:
   If you want to make the revert visible in a remote repository, push the changes:
   ```
   git push origin <branch-name>
   ```
   Replace `<branch-name>` with the name of the branch where you performed the revert.

After completing these steps, the changes introduced by the specific commit are effectively reverted in your local repository, and the revert is reflected in the commit history. The original commit remains in the history, but the changes it introduced are canceled out by the new revert commit.

Keep in mind that Git's revert command is a safe way to undo changes because it preserves the commit history and avoids rewriting the repository's history, which could cause issues for collaborators. If you want to remove a commit entirely from history, you can consider using other commands like `git reset` or `git rebase -i`. However, these commands should be used with caution, especially if the commits have already been pushed to a shared repository.

### How can I reset my changes ?

To reset your changes in Git, you can use the `git reset` command. This command allows you to move the current branch to a specific commit, effectively resetting the branch to that state. There are different options for resetting, depending on what you want to achieve:

1. **Resetting Uncommitted Changes**:
   If you have made changes to your files but haven't committed them yet and want to discard those changes, you can use the following command:
   ```
   git reset --hard HEAD
   ```
   This command will reset the working directory and the staging area to the state of the last commit (HEAD), effectively removing any uncommitted changes.

2. **Resetting to a Specific Commit**:
   If you want to reset your branch to a specific commit, including discarding any commits made after that point, use the following command:
   ```
   git reset --hard <commit-hash>
   ```
   Replace `<commit-hash>` with the hash of the commit you want to reset to. This will remove any commits made after the specified commit and reset your branch to that commit's state.

3. **Resetting Staged Changes**:
   If you have staged changes (using `git add`) but haven't committed them yet and want to unstage those changes, you can use the following command:
   ```
   git reset HEAD
   ```
   This command will remove the changes from the staging area and move them back to the working directory.

4. **Resetting and Preserving Changes**:
   If you want to undo the last commit but keep the changes from that commit in your working directory (so you can recommit them later), you can use the following command:
   ```
   git reset --soft HEAD~1
   ```
   This command will reset the branch to the parent of the last commit, keeping the changes from that commit in your working directory.

Remember that using `git reset` with the `--hard` option will permanently discard changes, and they cannot be easily recovered. Therefore, be cautious when using this command, especially if you haven't committed your changes yet.

Additionally, if you have already pushed the changes to a remote repository, avoid using `git reset` with `--hard`, as it will create issues for other developers who have already pulled those changes.

Always ensure you have a backup of your changes or that you are certain you want to discard them before using the `git reset` command. If you are unsure, you can use `git stash` to temporarily store your changes, or create a new branch to experiment with changes before resetting.

### What is GIT stash ?

In Git, `git stash` is a powerful command that allows you to save your changes in a temporary area called the "stash" without committing them to the repository. This is particularly useful when you have changes that are not ready for a commit or when you need to switch to a different branch to work on something else but don't want to commit the current changes.

The `git stash` command can be helpful in the following scenarios:

1. **Switching Branches**: When you have made changes in your current branch but want to switch to another branch, you can use `git stash` to save your changes and switch branches without committing them.

2. **Pulling and Merging**: Before pulling changes from a remote repository or merging another branch into your current branch, you can stash your local changes to avoid conflicts.

3. **Temporary Storage**: If you need to experiment with something quickly, you can stash your current changes, make the changes you want, and then come back to your original changes.

Here's how you can use `git stash`:

1. **Stash Your Changes**:
   To stash your changes, simply run the following command:
   ```
   git stash
   ```
   This command will save all your modified tracked files (changes in the working directory) and staged changes (changes in the staging area) to the stash. Your working directory will be clean after the stash.

2. **List Your Stashes**:
   You can list all your stashes using the following command:
   ```
   git stash list
   ```
   This will show a list of all stashes you've created, along with a unique identifier (stash reference).

3. **Apply the Stash**:
   To apply the most recent stash (the latest one you created), use the following command:
   ```
   git stash apply
   ```
   This command will apply the changes from the stash to your current branch's working directory and staging area. However, the stash itself will still exist in the stash list.

4. **Apply a Specific Stash**:
   If you have multiple stashes and want to apply a specific one, you can use its stash reference (e.g., `stash@{2}`):
   ```
   git stash apply stash@{2}
   ```

5. **Clear the Stash**:
   To remove a stash from the stash list (after applying it or no longer needing it), use the following command:
   ```
   git stash drop stash@{2}
   ```
   Replace `stash@{2}` with the stash reference of the stash you want to drop. If you don't specify a stash reference, `git stash drop` will remove the most recent stash.

Remember that stashing is a local operation and does not affect remote repositories. If you want to apply a stash to a different branch or push the changes to a remote repository, you need to perform the appropriate git operations (e.g., checkout to a different branch, commit the changes, or push the changes).

### What is GIT cherry-pick and how to use it ?
`git cherry-pick` is a Git command used to apply a specific commit from one branch onto another branch. It allows you to pick a single commit and apply it to your current branch, essentially copying the changes made in that commit and adding them as a new commit on your current branch. This can be useful when you want to selectively bring changes from one branch into another without merging the entire branch.

Here's how to use `git cherry-pick`:

1. **Switch to the Target Branch**:
   First, make sure you are on the branch where you want to apply the changes. You can use the `git checkout` command to switch to that branch.

   ```bash
   git checkout target_branch
   ```

2. **Identify the Commit to Cherry-Pick**:
   Find the commit hash of the commit you want to cherry-pick. You can use `git log` to view the commit history and identify the commit hash.

3. **Cherry-Pick the Commit**:
   Run the `git cherry-pick` command followed by the commit hash. This applies the changes from the specified commit to the current branch.

   ```bash
   git cherry-pick commit_hash
   ```

   Replace `commit_hash` with the actual commit hash you want to cherry-pick.

4. **Resolve Conflicts (if Any)**:
   If there are conflicts between the changes in the cherry-picked commit and the changes in your current branch, Git will pause the cherry-pick process and ask you to resolve the conflicts. After resolving conflicts, use `git add` to mark the resolved files as ready for commit, and then run `git cherry-pick --continue` to continue the cherry-pick process.

5. **Finish Cherry-Pick**:
   Once all conflicts are resolved and the cherry-pick is complete, you can commit the cherry-picked changes.

   ```bash
   git commit
   ```

6. **Clean Up** (Optional):
   After finishing the cherry-pick, you can delete the original branch if you no longer need it.

   ```bash
   git branch -d original_branch
   ```

Remember that `git cherry-pick` copies the changes from a single commit onto the current branch. If you need to apply a series of consecutive commits from one branch to another, consider using other methods like merging or rebasing.

Additionally, be cautious when cherry-picking commits that involve changes to the same lines of code that have been modified differently on the current branch. This can result in conflicts that need manual resolution.

Cherry-picking is a useful tool for bringing specific changes between branches, but keep in mind that it can alter commit history and potentially introduce inconsistencies if not used carefully. Always ensure that the changes you're cherry-picking make sense in the context of the target branch.

### What is GIT tag and how to create one ?
A Git tag is a reference to a specific commit in a Git repository. Tags are often used to mark significant points in the repository's history, such as release versions or important milestones. Unlike branches, tags don't move as new commits are added; they remain fixed to the specific commit they point to.

Tags come in two main types: lightweight tags and annotated tags.

1. **Lightweight Tags**:
   Lightweight tags are essentially just pointers to specific commits. They are created with the `git tag` command and do not contain additional metadata such as the tagger's name, email, or date.

   To create a lightweight tag, use the following command:

   ```bash
   git tag tag_name commit_hash
   ```

   Replace `tag_name` with the name you want to give to the tag and `commit_hash` with the hash of the commit you want to tag.

   Example:

   ```bash
   git tag v1.0.0 32a1b3f
   ```

2. **Annotated Tags**:
   Annotated tags include additional information such as the tagger's name, email, date, and an optional message. Annotated tags are recommended for releases and other significant points in the repository's history.

   To create an annotated tag, use the `-a` flag with the `git tag` command:

   ```bash
   git tag -a tag_name -m "Tag message" commit_hash
   ```

   Replace `tag_name` with the tag's name, `"Tag message"` with a descriptive message for the tag, and `commit_hash` with the hash of the commit you want to tag.

   Example:

   ```bash
   git tag -a v1.0.0 -m "Initial release" 32a1b3f
   ```

3. **Pushing Tags to Remote Repository**:
   By default, when you create a tag, it's only stored locally. If you want to share your tags with others or store them in a remote repository (such as on GitHub or GitLab), you need to push them explicitly:

   ```bash
   git push origin tag_name
   ```

   Replace `tag_name` with the name of the tag you want to push.

   Example:

   ```bash
   git push origin v1.0.0
   ```

Creating tags is a way to mark specific points in your Git history, which can be helpful for documentation, releases, and collaboration. They provide a snapshot of your codebase at a particular point in time. Remember that tags are immutable references, and once created, they do not move as new commits are added.

### What is Git reflog and when & how to use it ?
The Git reflog (reference log) is a built-in mechanism in Git that keeps a record of all reference changes in your repository, such as branch checkouts, commits, merges, and other operations that alter the commit history or move branches around. The reflog serves as a safety net to help you recover lost commits, branches, or changes that may have been accidentally removed or lost due to various actions.

In essence, the Git reflog is a history of where your HEAD and branch references have been pointing over time. It's especially useful in situations where you've made changes that you later want to undo or recover.

You can use Git reflog in various situations to recover lost commits, branches, or changes, and to understand the history of your repository. Here are some scenarios and examples of how to use Git reflog:

**Pre-requisite**
   ```bash
   # delete a remote branch
   git push <remote-name> --delete <branch-name>
   git push origin --delete feature/xyz
   # delete a local brnahc
   git branch -d feature/xyz
   ```


1. **Recovering Deleted Branch**:

   If you accidentally deleted a branch and want to recover it:

   ```bash
   # Find the commit hash of the branch before deletion in the reflog
   git reflog
   # Create a new branch at that commit
   git branch recovered_branch_name commit_hash
   ```

2. **Recovering Lost Commits**:

   If you want to recover commits that were accidentally deleted:

   ```bash
   # Find the commit hash of the lost commit in the reflog
   git reflog
   # Create a new branch or reset to that commit
   git branch recovered_branch_name commit_hash
   ```

3. **Undoing Mistaken Operations**:

   If you performed an operation that you regret, like a hard reset, and you want to undo it:

   ```bash
   # Find the commit hash before the operation in the reflog
   git reflog
   # Reset back to that commit
   git reset --hard commit_hash
   ```

4. **Troubleshooting**:

   If you're facing unexpected issues and want to understand how you arrived at a certain state:

   ```bash
   # View the reflog to trace the recent operations
   git reflog
   ```

5. **Understanding History**:

   If you're trying to track changes and understand how different branches or commits have been moving over time:

   ```bash
   # View the reflog to see the history of reference changes
   git reflog
   ```

Remember the basic steps to use Git reflog:

1. Open a terminal or command prompt.
2. Navigate to your Git repository using the `cd` command.
3. Run the command `git reflog`.

This will show you a chronological list of reference changes, their corresponding commit hashes, dates, and descriptions of the operations performed.

However, while Git reflog is a powerful tool, it's important to exercise caution:

- Reflog entries are eventually pruned, so don't rely on it for long-term history.
- Make sure you're familiar with Git's commands and their effects before using reflog to undo or recover changes.
- Create backups or branches before attempting to recover or undo operations.

In summary, Git reflog is a handy tool to recover from mistakes, troubleshoot issues, and understand how your repository's references have changed over time. Use it when you need to pinpoint lost changes, branches, or understand the history of operations.
